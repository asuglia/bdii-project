# Database Management Systems II project #

This repository contains the code for the project of the university course called [Database systems II](http://www.di.uniba.it/~ceci/micFiles/courses/bdii/bdii.htm) for the master degree in *Informatics* of the *University of Bari*. It's a simple search engine which grants to a generic user to make a search using a full text query and for registred users to look for their past searches too.

It contains the whole web application implemented using HTML5 and Javascript and the backend application implemented using the [Ninja Framework](http://www.ninjaframework.org/).

## Dependencies and settings 

The whole project has been developed using the Maven Framework so, using the POM file present in the main directory you will be able to compile and start the web application. The Ninja framework uses a **Maven goal** in order to start a web server that automatically deploys and starts the web application. You can find a detailed explanation of this feature [at this page](http://www.ninjaframework.org/documentation/basic_concepts/super_dev_mode.html) 