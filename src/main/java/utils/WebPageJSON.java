package utils;

/**
 * Struttura di supporto per scambiare oggetti JSON
 * tra la web application e il server
 */
public class WebPageJSON {
    public String website;

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
