package utils;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.RDFNode;

import java.io.IOException;
import java.util.logging.Logger;


/**
 * Classe di supporto impiegata per eseguire query SPARQL e recuperare
 * le informazioni da specifici endpoint della Linked Open Data Cloud
 */
public class SPARQLClient {
    private static Logger currLogger = Logger.getLogger(SPARQLClient.class.getName());

    /**
     * Crea una query ben formata a partire dalla rappresentazione testuale specificata
     * in input
     *
     * @param query rappresentazione testuale della query
     * @return forma strutturata di una query SPARQL
     */
    public Query createQuery(String query) {
        return QueryFactory.create(query);
    }

    /**
     * Esegue la query specificata sull'endpoint del quale viene specificato
     * l'url.
     *
     * @param query query SPARQL da eseguire
     * @param endpoint URL dell'endpoint
     * @return insieme di triple rispondenti alla query
     * @throws IOException errore nell'esecuzione della query
     */
    public ResultSet executeQuery(Query query, String endpoint) throws IOException {
        QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint, query);
        return exec.execSelect();

    }

    /**
     * Restituisce il nome della risorsa specificata in lingua inglese
     *
     * @param node riferimento alla risorsa RDF
     * @param endpoint endpoint di riferimento
     * @param prefix prefisso da impiegare per l'endpoint di riferimento
     * @return nome in lingua inglese della risorsa
     */
    public String takeEnglishLabel(RDFNode node, String endpoint, String prefix) {
        String resourceURI = node.asResource().toString(),
                query = prefix + "SELECT (str(?resLabel) as ?label) WHERE { <" +
                resourceURI + "> " + " rdfs:label ?resLabel }";

        currLogger.info(query);

        QueryExecution queryExec = QueryExecutionFactory.sparqlService(endpoint, QueryFactory.create(query));

        ResultSet results = queryExec.execSelect();

        String resourceStrValue = null;

        while(results.hasNext()) {
            QuerySolution sol = results.nextSolution();
            resourceStrValue = sol.get("?label").toString();
        }

        if(resourceStrValue == null) {
            resourceStrValue = resourceURI.substring(resourceURI.lastIndexOf("/"), resourceURI.length());
        }

        return resourceStrValue;
    }
}
