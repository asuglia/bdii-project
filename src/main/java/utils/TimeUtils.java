package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by asuglia on 2/21/15.
 */
public class TimeUtils {
    private static final String TIME_FORMAT = "kk:mm:ss";
    private static final SimpleDateFormat simpleFormatter = new SimpleDateFormat(TIME_FORMAT);


    public static String getCurrentTime(Calendar calendar) {
        return simpleFormatter.format(calendar.getTime());
    }
}
