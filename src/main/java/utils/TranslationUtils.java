package utils;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import ninja.i18n.Messages;
import ninja.utils.NinjaProperties;
import org.apache.commons.lang3.StringUtils;

/**
 * Classe di utility che impiega gli strumenti messi a disposizione dal
 * framework per poter rilevare quale sia la lingua attualmente impiegata
 * e restituire i messaggi localizzati rispetto a tale lingua
 */
@Singleton
public class TranslationUtils {
    private Messages messages;
    private NinjaProperties ninjaProperties;

    @Inject
    public TranslationUtils(Messages messages, NinjaProperties ninjaProperties) {
        this.messages = messages;
        this.ninjaProperties = ninjaProperties;
    }

    private String get(String key, Object [] paramters) {
        Optional<String> language = Optional.of(getDefaultLanguage());
        return messages.getWithDefault(key, "Language: " + language + " - Missing translation for key: " + key, language, paramters);
    }

    public String get(String key) {
        return get(key, new Object[]{});
    }

    private String getDefaultLanguage() {
        String language = ninjaProperties.get("application.language");
        if (StringUtils.isNotBlank(language)) {
            if (("en").equalsIgnoreCase(language)) {
                language = "en";
            } else if (("it").equalsIgnoreCase(language)) {
                language = "it";
            }
        } else {
            language = "en";
        }

        return language;
    }
}
