function onClickPageRead(searchResult) {

    var DataSite = function (searchResults) {
        this.website = searchResults.getAttribute("href");
    };

    var data = new DataSite(searchResult);
    var websiteUrl = data.website;

    console.log(data);

    $.ajax({
        type: "POST",
        url: "/readpage",
        crossDomain: true,
        async: true,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            window.location.href = websiteUrl;
        },
        error: function(data) {
            console.log("ERROR");
            console.log(data.status);
        }

    });

    return false;

}