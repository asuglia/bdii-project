function checkUrl(webpageUrl, errWrongUrl) {

    var re = new RegExp("http://it.wikipedia.org/wiki/.*|https://it.wikipedia.org/wiki/.*");

    var OK = re.exec(webpageUrl.value);

    if (!OK)
        webpageUrl.setCustomValidity(errWrongUrl);

    else
        webpageUrl.setCustomValidity("");

    return OK;
}

var snackbarElement = null;

function insertNewPage(errWrongUrl, loadMsg, errMsg) {
    var webpageUrlElem = document.getElementById("webpageURL");
    if (checkUrl(webpageUrlElem, errWrongUrl)) {
        var webpage = webpageUrlElem.value;

        var DataSite = function (webpage) {
            this.website = webpage;
        };

        var data = new DataSite(webpage);

        console.log(data);

        var optionsSnackbar = {
            content: loadMsg, // text of the snackbar
            style: "info-snack", // add a custom class to your snackbar
            timeout: 0, // time in milliseconds after the snackbar autohides, 0 is disabled,
            htmlAllowed: true // allows HTML as content value
        };

        snackbarElement = $.snackbar(optionsSnackbar);

        $.ajax({
            type: "POST",
            url: "/newpage",
            crossDomain: true,
            async: true,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                snackbarElement.snackbar("hide");
                console.log("SUCCESS");
                console.log(data);
                showSnackbar(data.msg, false);

            },
            error: function(data) {
                snackbarElement.snackbar("hide");
                console.log("ERROR");
                console.log(data);
                if(data.msg != null) {
                    showSnackbar(errMsg, true)
                } else {
                    showSnackbar(data.msg, true);
                }
            }

        });
    }

    return false;

}

function showSnackbar(content, isError) {
    var optionsSnackbar = null;

    if(isError) {
        optionsSnackbar = {
            content: content, // text of the snackbar
            style: "well-alert", // add a custom class to your snackbar
            timeout: 2000
        };
    } else {
        optionsSnackbar = {
            content: content, // text of the snackbar
            style: "info-snack", // add a custom class to your snackbar
            timeout: 2000
        };
    }

    snackbarElement = $.snackbar(optionsSnackbar);

}