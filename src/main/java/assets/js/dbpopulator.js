var TableMapper = function (dbTableNameId, genreId, tupleLimitId, tableBodyId) {
    var tableBody = document.getElementById(tableBodyId);

    this.tableName = document.getElementById(dbTableNameId).value;
    this.genre = document.getElementById(genreId).value;
    this.tupleLimit = document.getElementById(tupleLimitId).value;
    this.mappingAttributes = {};

    if (tableBody.children.length != 0) {
        var attributeList = tableBody.children;

        // loop for each <tr> and get <td> element from it
        for (var i = 0; i < attributeList.length; i++) {
            this.mappingAttributes[attributeList[i].children[0].innerText] = attributeList[i].children[1].innerText;
        }
    } else {
        this.mappingAttributes = null;
    }

};

var snackbarElement = null;

function clearForm() {
    $("#dbTableName").val("");
    $("#limitTuple").val("");
    $("#dbField").val("");
    $("#resName").val("");

}

function makeHTTPRequest(url, data) {


    $.ajax({
        type: "POST",
        url: "/dbpopulator",
        crossDomain: "true",
        async: true,
        // The key needs to match your method's input parameter (case-sensitive).
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
            snackbarElement.snackbar("hide");
            console.log("SUCCESS");
            console.log(data);
            clearForm();
            clearTable();
            showSnackbar(data.msg, false);

        },
        error: function(data) {
            snackbarElement.snackbar("hide");
            console.log("ERROR");
            console.log(data);
            clearForm();
            clearTable();
            $("#dbpopForm").reload();
            showSnackbar(data.msg, true);
        }

    });

    return false;
}


function serveFormData(dbTableNameId, genreId, tupleLimitId, tableBodyId, errMsg, loadMsg, okMsg) {
    var optionsSnackbar = null;

    var table = new TableMapper(dbTableNameId, genreId, tupleLimitId, tableBodyId);

    if (table.mappingAttributes == null) {
        optionsSnackbar = {
            content: errMsg.dbpop_noattr, // text of the snackbar
            style: "well-danger", // add a custom class to your snackbar
            timeout: 100, // time in milliseconds after the snackbar autohides, 0 is disabled,
            htmlAllowed: true // allows HTML as content value
        };
        snackbarElement = $.snackbar(optionsSnackbar);
    } else {
        optionsSnackbar = {
            content: loadMsg, // text of the snackbar
            style: "info-snack", // add a custom class to your snackbar
            timeout: 0, // time in milliseconds after the snackbar autohides, 0 is disabled,
            htmlAllowed: true // allows HTML as content value
        };

        snackbarElement = $.snackbar(optionsSnackbar);

        var formData = JSON.stringify(table);

        makeHTTPRequest("/dbpopulator", formData, errMsg, okMsg);

        return false;

    }
}

function insertNewAttribute() {
    var dbField = document.getElementById("dbField").value,
        resName = document.getElementById("resName").value;

    if (dbField !== "" && resName !== "") {
        var attributeList = document.getElementById("attributeList-body");

        var tableRow = document.createElement('tr');
        var tableDbField = document.createElement('td'),
            tableResName = document.createElement('td');

        tableDbField.innerText = dbField;
        tableResName.innerText = resName;

        tableRow.appendChild(tableDbField);
        tableRow.appendChild(tableResName);
        tableRow.addEventListener("click", function (e) {

            if (e.currentTarget.attributes["class"] == null)
                tableRow.setAttribute("class", "active");
            else
                tableRow.removeAttribute("class");

            for (var i = 0; i < attributeList.children.length; i++) {
                if (attributeList.children[i] != tableRow) {
                    attributeList.children[i].removeAttribute("class");
                }

            }
        });

        attributeList.appendChild(tableRow);
    }

}

function clearTable() {
    var attributeList = document.getElementById("attributeList-body").children;

    for (var i = attributeList.length; i--;) {
        attributeList[i].remove();
    }

}

function removeSelectedAttribute() {
    var attributeList = document.getElementById("attributeList-body").children;
    var selectedRow = null;

    for (var i = 0; i < attributeList.length; i++) {
        var classAttribute = attributeList[i].getAttribute("class");
        if (classAttribute != null && classAttribute == "active") {
            selectedRow = attributeList[i];
        }
    }

    // delete the selected row
    if (selectedRow != null)
        selectedRow.remove();

}


function showSnackbar(content, isError) {
    var optionsSnackbar = null;

    if(isError) {
        optionsSnackbar = {
            content: content, // text of the snackbar
            style: "well-alert", // add a custom class to your snackbar
            timeout: 2000
        };
    } else {
        optionsSnackbar = {
            content: content, // text of the snackbar
            style: "info-snack", // add a custom class to your snackbar
            timeout: 2000
        };
    }

    snackbarElement = $.snackbar(optionsSnackbar);

}