package filters;

import ninja.*;
import ninja.session.Session;

/**
 * Classe che rappresenta un filtro che impedisce agli utenti che non hanno effettuato
 * il login di effettuare operazioni da utente standard.
 */
public class RegistredUserFilter implements Filter {
    private static final String USERNAME_SESSION_ID = "userName";

    @Override
    public Result filter(FilterChain filterChain, Context context) {
        Session currSession = context.getSession();

        if (currSession == null ||
                currSession.get(USERNAME_SESSION_ID) == null) {
            return Results.forbidden().html().template("views/system/403forbidden.ftl.html").html();
        }

        return filterChain.next(context);

    }
}
