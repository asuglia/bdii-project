package filters;

import dao.EntityDAO;
import dao.UserAnnotation;
import dao.UserDAO;
import dao.HandlerData;
import com.google.inject.Inject;
import ninja.*;
import ninja.session.Session;

/**
 * Classe che rappresenta un filtro che impedisce a degli utenti che
 * non possiedono delle credenziali da amministratore di effettuare
 * operazioni con privilegi avanzati.
 */
public class LoggedAdmin implements Filter {
    @Inject
    @UserAnnotation
    EntityDAO userDAO;

    @Override
    public Result filter(FilterChain filterChain, Context context) {
        Session session = context.getSession();

        if (session != null) {
            String userName = session.get(UserDAO.FRONT_USERNAME);
            if(userName != null) {
                HandlerData data = new HandlerData();
                data.setAttribute(UserDAO.OPERATION_CODE, "checkAdmin");
                data.setAttribute(UserDAO.FRONT_USERNAME, userName);
                HandlerData response = userDAO.get(data);

                if(response.getAttribute(UserDAO.ERRCODE) == null && (boolean) response.getAttribute(UserDAO.FRONT_ROOT)) {
                    return filterChain.next(context);
                }

            }
        }

        return Results.forbidden().html().template("views/system/403forbidden.ftl.html").html();
    }
}
