package dbpop;

import dao.EntityDAO;
import dao.HandlerData;
import utils.SPARQLClient;
import com.google.inject.Inject;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import dbpop.endpoint.IEndpoint;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Classe che definisce il componente principale del motore di riempimento
 * del database, che sfrutta le informazioni provenienti da un endpoint
 * della Linked Open Data Cloud
 */
public class DBPopulationEngine {
    private EntityDAO entityDAO;
    private final Map<String, EntityDAO> daoBinding;
    private Properties mappingAttribute;
    private String sparqlEndpoint;
    private String endpointPrefix;
    private static final String GENRE_KEY = "__genre";
    private static final String TUPLE_LIMIT = "__tuple_limit";
    private static final String TABLE_KEY = "__table_name";
    private final IEndpoint endpoint;
    private String genreProperty;
    private SPARQLClient sparqlClient;

    private Logger currLogger = Logger.getLogger(DBPopulationEngine.class.getName());

    /**
     * Crea un database populator ricevendo in input il DAO relativo alla tabella
     * che si vuole popolare.
     *
     * @param daoImplementations classi che il motore di riempimento è in grado di gestire
     */
    @Inject public DBPopulationEngine(final Map<String, EntityDAO> daoImplementations, final IEndpoint endpoint) {
        this.daoBinding = daoImplementations;
        this.sparqlClient = new SPARQLClient();
        this.endpoint = endpoint;

    }

    /**
     * Configura il motore di riempimento impiegando il file properties il cui
     * nome è specificato come parametro.
     *
     * @param mappingFile percorso al file di properties
     * @throws IOException
     */
    public void initConfiguration(String mappingFile) throws IOException, InvalidConfigurationException {
        Properties tempProp = new Properties();
        tempProp.load(new FileReader(mappingFile));
        checkConfigurationValidity(tempProp);
        this.mappingAttribute = tempProp;
    }

    /**
     * Configura il motore di riempimento utilizzando l'oggetto di configurazione
     * specificato
     *
     * @param configuration oggetto di configurazione corrente
     * @throws InvalidConfigurationException nel caso in cui la configurazione corrente sia invalida
     */
    public void initConfiguration(DBPopulationEngineConf configuration) throws InvalidConfigurationException {
        checkConfigurationValidity(configuration);
        mappingAttribute = new Properties();
        mappingAttribute.setProperty(GENRE_KEY, configuration.genre);
        mappingAttribute.setProperty(TUPLE_LIMIT, configuration.tupleLimit);

        Map<String, String> mappingAttributeJson = configuration.mappingAttributes;

        for (String key : mappingAttributeJson.keySet()) {
            mappingAttribute.put(key, mappingAttributeJson.get(key));
        }
    }

    /**
     * Metodo che verifica se i parametri di configurazione specificati rispettino i vincoli
     * imposti dal motore di riempimento.
     *
     * Il campo relativo al genere supporta i seguenti valori:
     *      - "film"
     *      - "actors"
     *      - "automobile"
     *
     * Il campo relativo al numero massimo di tuple può assumere un valore compreso tra 1 e 2000
     * (estremi inclusi).
     *
     * @param configuration la configurazione corrente
     * @throws InvalidConfigurationException parametri di configurazione errati
     */
    private void checkConfigurationValidity(Properties configuration) throws InvalidConfigurationException {
        String genre = configuration.getProperty(GENRE_KEY), tupleLimit = configuration.getProperty(TUPLE_LIMIT),
                tableName = configuration.getProperty(TABLE_KEY);

        switch (genre) {
            case "film":
            case "actors":
            case "automobile":
                break;
            default:
                throw new InvalidConfigurationException("invalid genre - ");
        }

        if(Integer.parseInt(tupleLimit) > 2000)
            throw new InvalidConfigurationException("invalid tuple limit - " + tupleLimit);

        this.entityDAO = daoBinding.get(tableName);
        if(this.entityDAO == null)
            throw new InvalidConfigurationException("invalid table name - " + tableName);
    }
    /**
     * Metodo che verifica se i parametri di configurazione specificati rispettino i vincoli
     * imposti dal motore di riempimento.
     *
     * Il campo relativo al genere supporta i seguenti valori:
     *      - "film"
     *      - "actors"
     *      - "automobile"
     *
     * Il campo relativo al numero massimo di tuple può assumere un valore compreso tra 1 e 2000
     * (estremi inclusi).
     *
     * @param configuration la configurazione corrente
     * @throws InvalidConfigurationException parametri di configurazione errati
     */
    private void checkConfigurationValidity(DBPopulationEngineConf configuration) throws InvalidConfigurationException {
        switch (configuration.genre) {
            case "film":
            case "actors":
            case "automobile":
                break;
            default:
                throw new InvalidConfigurationException("invalid genre - " + configuration.genre);
        }

        if(Integer.parseInt(configuration.tupleLimit) > 2000)
            throw new InvalidConfigurationException("invalid tuple limit - " + configuration.tupleLimit);

        this.entityDAO = daoBinding.get(configuration.tableName);
        if(this.entityDAO == null)
            throw new InvalidConfigurationException("invalid table name - " + configuration.tableName);
    }

    /**
     * Avvia il popolamento del database con la configurazione corrente impostata.
     * E' fondamentale aver inizializzato correttamente il motore di riempimento
     * altrimenti si incorrerà in errori nel corso dell'esecuzione.
     *
     * @throws IOException errori nel corso dell'esecuzione del popolamento del database
     */
    public void startDBPopulation() throws IOException {
        selectEndpoint();
        retrieveResources();
        currLogger.info("DB population finished!");

    }

    /**
     * Seleziona i parametri dell'endpoint che verrà impiegato per poter
     * recuperare le informazioni necessarie per il popolamento della tabella
     *
     * N.B.
     * Attualmente l'unico endpoint disponibile risulta essere DBpedia
     */
    private void selectEndpoint() {
        sparqlEndpoint = endpoint.getEndpoint();
        endpointPrefix = endpoint.getEndpointPrefix();

        switch (mappingAttribute.getProperty(GENRE_KEY)) {
            case "film":
                genreProperty = "dbpedia-owl:Film";
                break;
            case "actors":
                genreProperty = "dbpedia-owl:Actor";
                break;
            case "automobile":
                genreProperty = "dbpedia-owl:Automobile";
                break;
            default:
                break;
        }
    }

    /**
     * Genera la query da impiegare per poter recuperare tutte le informazioni
     * relative alle risorse appartenenti ad un determinato genere
     * @return query in linguaggio SPARQL in formato testuale
     */
    private String generateQuery() {
        String commonTemplate = endpointPrefix + "SELECT %s WHERE {?res rdf:type %s; %s} LIMIT " + mappingAttribute.getProperty(TUPLE_LIMIT);

        StringBuilder fieldsString = new StringBuilder(""), propertyStatement = new StringBuilder("");

        for (String key : mappingAttribute.stringPropertyNames()) {
            if (!key.startsWith("__")) {
                fieldsString.append("?").append(key).append(" ");
                propertyStatement.append(mappingAttribute.getProperty(key)).append(" ").append("?").append(key).append(";");
            }
        }

        return String.format(commonTemplate, fieldsString.toString(), genreProperty, propertyStatement.toString());
    }

    /**
     * Interroga l'endpoint impiegando la query generata sulla base dei parametri di
     * configurazione impostati. Dopo aver recuperato tutti i risultati, provvede ad inserirli
     * all'interno del database rispettando il mapping definito tra gli attributi.
     *
     * @throws IOException rappresenta una situazione d'errore che può insorgere nell'inserimento
     * dei risultati del database, nell'interrogazione dell'endpoint o nel parsing della query
     */
    private void retrieveResources() throws IOException {
        String query = generateQuery();

        currLogger.info(query);

        Query realQuery = sparqlClient.createQuery(query);

        ResultSet results = sparqlClient.executeQuery(realQuery, sparqlEndpoint);
        QuerySolution solution = null;
        HandlerData data = null;

        int i = 0;

        while (results.hasNext()) {
            data = new HandlerData();
            solution = results.nextSolution();
            for (String key : mappingAttribute.stringPropertyNames()) {
                if (!key.startsWith("__")) {
                    RDFNode node = solution.get(String.valueOf("?" + key));
                    if (node.isLiteral()) {
                        data.setAttribute(key, node.asLiteral().getString());
                    } else {
                        data.setAttribute(key, sparqlClient.takeEnglishLabel(node, sparqlEndpoint, endpointPrefix));
                    }
                }
            }

            currLogger.info(data.toString());
            data.setAttribute("OP", "populateDB");
            entityDAO.insert(data);
            i++;

            myWait(20);
            if (i % 50 == 0)
                myWait(20);

        }


    }


    /**
     * Metodo impiegato per poter attendere un certo numero di secondi.
     *
     * @param sec secondi di attesa
     */
    private void myWait(int sec) {
        //wait
        long cm = System.currentTimeMillis();
        while ((System.currentTimeMillis() - cm) < sec * 1000) ;

    }

}


