package dbpop;

/**
 * Classe che rappresenta una situazione di errore relativa ad una errata
 * configurazione specificata per il motore di riempimento del database
 */
public class InvalidConfigurationException extends Exception {
    private String message;

    public InvalidConfigurationException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return "Wrong parameters specified: " + message;
    }
}
