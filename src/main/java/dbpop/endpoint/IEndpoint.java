package dbpop.endpoint;

/**
 * Interfaccia che rappresenta i metodi per accedere ai parametri
 * di configurazione degli endpoint
 */
public interface IEndpoint {
    public String getEndpoint();
    public String getEndpointPrefix();
}
