package dbpop.endpoint;

/**
 * Classe che rappresenta i parametri di configurazione dell'endpoint
 * di DBpedia, necessari per poterlo interrogare
 */
public class DBpediaEndpoint implements IEndpoint {
    private static final String endpoint = "http://live.dbpedia.org/sparql";
    private static final String endpointPrefix = "PREFIX dbpedia:<http://dbpedia.org/resource/> \n" +
            "PREFIX dbpedia-owl:<http://dbpedia.org/ontology/>\n" +
            "PREFIX dbpprop:<http://dbpedia.org/property/>\n" +
            "PREFIX dc:<http://purl.org/dc/elements/1.1/>\n" +
            "PREFIX dcterms:<http://purl.org/dc/terms/>\n" +
            "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" +
            "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";

    DBpediaEndpoint() {}

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public String getEndpointPrefix() {
        return endpointPrefix;
    }
}
