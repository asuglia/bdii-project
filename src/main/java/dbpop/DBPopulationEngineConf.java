package dbpop;

import java.util.Map;

/**
 * Classe POJO che rappresenta i parametri di configurazione necessari
 * per poter inizializzare correttamente il motore di riempimento
 */
public class DBPopulationEngineConf {
    public String tableName;
    public String genre;
    public String tupleLimit;
    public Map<String, String> mappingAttributes;

    /**
     * Controlla se tutti i parametri di configurazione non siano nulli
     *
     * @return true se i parametri sono non nulli, false altrimenti
     */
    public boolean isNotNull() {

        for (String key : mappingAttributes.keySet()) {
            if (key.equals("") || mappingAttributes.get(key).equals(""))
                return false;
        }

        return (tableName != null && !tableName.equals("")) &&
                (genre != null && !tableName.equals(""));
    }


    /**
     * Trasforma la stringa che rappresenta il genere,
     * nella codifica impiegata dalla configurazione.
     *
     * La trasformazione viene fatta sulla base del linguaggio impiegato
     * dall'utente
     *
     * @param language linguaggio dell'utente
     */
    public void fixGenre(String language) {
        switch (language) {
            case "en-US":
            case "en-EN":
                fixGenreEnglish();
                break;
            case "it-IT":
                fixGenreItalian();
                break;
        }

    }

    /**
     * Effettua la trasformazione per una stringa
     * che rappresenta il genere, scritta in italiano.
     */
    private void fixGenreItalian() {
        switch (genre) {
            case "Attori":
                genre = "actor";
                break;
            case "Automobili":
                genre = "automobile";
                break;
            case "Film":
                genre = "film";
                break;
        }
    }

    /**
     * Effettua la trasformazione per una stringa
     * che rappresenta il genere, scritta in inglese.
     */
    private void fixGenreEnglish() {
        switch (genre) {
            case "Actor":
                genre = "actor";
                break;
            case "Automobile":
                genre = "automobile";
                break;
            case "Film":
                genre = "film";
                break;
        }
    }
}
