package conf;

import com.google.inject.Provider;
import ninja.utils.NinjaProperties;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by asuglia on 2/22/15.
 */
public class DBProvider implements Provider<Connection> {

    private final NinjaProperties properties;

    @Inject
    public DBProvider(NinjaProperties properties)
    {
        this.properties = properties;
        try {
            Class.forName(properties.get("database.driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection get() {

        try {
            return DriverManager.getConnection(properties.get("database.url"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
