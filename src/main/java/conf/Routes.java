package conf;

import controllers.AdminController;
import controllers.AsyncController;
import controllers.LoginController;
import controllers.UserController;
import ninja.AssetsController;
import ninja.Router;
import ninja.application.ApplicationRoutes;

/***
 * Classe che eredita dalla classe {@link ninja.application.ApplicationRoutes}
 * acquisiendo le specifiche funzionalità necessarie per poter gestire il flusso di richieste
 * e la loro risoluzione che viene effettuata dal specifici metodi implementati.
 */
public class Routes implements ApplicationRoutes {

    /**
     * Provvede ad inizializzare il componente che redireziona le richieste provenienti
     * dall'interfaccia grafica verso specifiche componenti del sistema.
     *
     * In questa fase di inizializzazione viene definito un vero mapping tra un URL path
     * ed un particolare metodo dei Controller definiti.
     * */
    @Override
    public void init(Router router) {


        /////////////////////////////////////////////////////////////////////
        // Common user operations
        /////////////////////////////////////////////////////////////////////

        router.GET().route("/user/home").with(UserController.class, "home");
        //TODO: define common user operations
        router.GET().route("/search").with(UserController.class, "search");
        router.POST().route("/user/sessions/delete").with(UserController.class, "deleteSessions");
        router.GET().route("/user/sessions").with(UserController.class, "sessions");
        router.POST().route("/user/sessions/{session_id}").with(UserController.class, "getSession");
        router.POST().route("/readpage").with(AsyncController.class, "readPage");



        /////////////////////////////////////////////////////////////////////
        // Admin user operations
        /////////////////////////////////////////////////////////////////////
        //TODO: define admin user operations
        router.GET().route("/dbpopulator").with(AdminController.class, "dbPopulator");
        router.POST().route("/dbpopulator").with(AsyncController.class, "postDbPopulator");
        router.GET().route("/newpage").with(AdminController.class, "insertPage");
        router.POST().route("/newpage").with(AsyncController.class, "postInsertPage");
        router.GET().route("/analysis").with(AdminController.class, "analysis");

        /////////////////////////////////////////////////////////////////////
        // Login operations
        /////////////////////////////////////////////////////////////////////
        router.POST().route("/login").with(LoginController.class, "loginPost");
        router.GET().route("/login").with(LoginController.class, "login");
        router.GET().route("/logout").with(LoginController.class, "logout");
        router.GET().route("/signup").with(LoginController.class, "signup");
        router.POST().route("/signup").with(LoginController.class, "signUpPost");


        ///////////////////////////////////////////////////////////////////////
        // Assets (pictures / javascript)
        ///////////////////////////////////////////////////////////////////////
        router.GET().route(".*/assets/webjars/{fileName: .*}").with(AssetsController.class, "serveWebJars");
        router.GET().route(".*/assets/{fileName: .*}").with(AssetsController.class, "serveStatic");

        ///////////////////////////////////////////////////////////////////////
        // Index - shows index page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/").with(UserController.class, "index");
        router.GET().route("/index").with(UserController.class, "index");
    }

}
