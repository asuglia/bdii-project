package conf;

import dao.*;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.MapBinder;

import dao.EntityDAO;
import dao.UserAnnotation;
import dbpop.endpoint.DBpediaEndpoint;
import dao.UserDAO;
import dbpop.endpoint.IEndpoint;
import ninja.utils.NinjaProperties;
import services.DBConnectionService;
import wiki.model.SiteData;
import wiki.model.WikipediaData;
import wiki.tokenizer.NaiveTokenizer;
import wiki.tokenizer.PageTokenizer;

import javax.inject.Inject;
import java.sql.Connection;


/**
 * Rappresenta il modulo del sistema che si occupa del caricamento
 * di tutti i moduli del sistema. Nello specifico impiega la tecnica della
 * dependency injection per istanziare in maniera opportuna tutte gli oggetti
 * delle classi impiegate nei vari moduli del sistema.
 * */
@Singleton
public class Module extends AbstractModule {
    @Inject
    private NinjaProperties properties;

    /**
     * Provvede ad inizializzare l'ORM per l'accesso al database ed effettua il binding
     * di ogni singola classe astratta con la rispettiva implementazione concreta
     * adottata dal sistema.
     * */
    protected void configure() {
        // Database Ebean
        // This installs the NinjaModule and handles the lifecycle
        //install(new NinjaEbeanModule());
        bind(Connection.class).toProvider(DBProvider.class);
        bind(DBConnectionService.class);
        // Inject the correct DAO entity that is used in order to
        // execute the DB operations
        // DAO class used in order to complete operation on entities
        // All the EntityDAO should have their own annotation
        // DAO class used in order to complete operation on user (login, logout, sign-up)
        bind(EntityDAO.class).annotatedWith(UserAnnotation.class).to(UserDAO.class);

        bind(EntityDAO.class).annotatedWith(SessionAnnotation.class).to(SessionDAO.class);

        bind(EntityDAO.class).annotatedWith(RegSessionAnnotation.class).to(RegistredSessionDAO.class);
        bind(EntityDAO.class).annotatedWith(WebpageAnnotation.class).to(WebpageDAO.class);


        bind(IEndpoint.class).to(DBpediaEndpoint.class);
        bind(PageTokenizer.class).to(NaiveTokenizer.class);
        bind(SiteData.class).to(WikipediaData.class);

        MapBinder<String, EntityDAO> mapbinder = MapBinder.newMapBinder(binder(), String.class, EntityDAO.class);

    }

}
