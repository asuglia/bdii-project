package controllers;

import dao.EntityDAO;
import dao.RegSessionAnnotation;
import dao.SessionAnnotation;
import dao.UserAnnotation;
import filters.RegistredUserFilter;
import dao.HandlerData;
import com.google.inject.Inject;
import models.RegistredSession;
import models.WebPages;
import ninja.*;

import com.google.inject.Singleton;
import ninja.params.Param;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import ninja.session.Session;

import java.util.*;

import utils.TimeUtils;


/**
 * Controller che contiene tutti i metodi necessari per soddisfare le richieste
 * effettuate da un utente con privilegi standard.
 */
@Singleton
public class UserController {
    public static final String USERNAME_SESSION = "userName";
    public static final String SEARCH_SESSION = "searchSession";
    private static final String SEARCH_QUERY_SESSION = "searchQuery";
    @Inject
    @SessionAnnotation
    private EntityDAO anonimSessionDAO;
    @Inject
    @RegSessionAnnotation
    private EntityDAO userSessionDAO;
    @Inject
    @UserAnnotation
    private EntityDAO userDAO;

    /**
     * Metodo necessario per rispondere ad una richiesta GET impiegata
     * per ritornare la pagina principale dell'applicazione
     * @return la home page della web application
     */
    public Result index() {
        return Results.html();

    }

    /**
     * Restituisce la pagina principale dell'utente che ha effettuato
     * il login. L'accesso alla pagina principale non è consentito ad utente
     * che non hanno effettuato il login.
     *
     * @param context contesto della richiesta corrente
     * @return La home page dell'utente o una pagina di errore in caso di
     * accesso negato (403)
     */
    @FilterWith(RegistredUserFilter.class)
    public Result home(Context context) {
        String userName = context.getSession().get(USERNAME_SESSION);
        HandlerData data = new HandlerData();
        data.setAttribute("OP", "checkAdmin");
        data.setAttribute(USERNAME_SESSION, userName);

        data = userDAO.get(data);

        return ((boolean) data.getAttribute("isAdmin"))
                ? Results.html().template("views/AdminController/home.admin.ftl.html").render("userName", userName)
                :Results.html().render("userName", userName);
    }

    /**
     * Cancella tutte le sessioni registrate per l'utente corrente.
     *
     * @param currContext contesto della richiesta
     * @return ritorna alla pagina che visualizza le informazioni sulle sessioni dell'utente
     */
    @FilterWith(RegistredUserFilter.class)
    public Result deleteSessions(Context currContext){
        Result results = Results.redirect("/user/sessions"); // redirect there

        String userName = currContext.getSession().get(USERNAME_SESSION);
        HandlerData data = new HandlerData();

        data.setAttribute("OP", "deleteSessionsForUser");
        data.setAttribute(USERNAME_SESSION, userName);

        HandlerData response = userSessionDAO.delete(data);

        FlashScope scope = currContext.getFlashScope();

        if(response.getAttribute("errcode") != null) {
            scope.error("sessions.error");
        }

        return results;
    }

    /**
     * Restituisce la lista delle sessioni effettuate
     * da uno specifico utente.
     *
     * @param currContext contesto della richiesta
     * @return pagina con le informazioni relative alle sessioni svolte dall'utente specificato
     */
    @FilterWith(RegistredUserFilter.class)
    public Result sessions(Context currContext) {
        String userName = currContext.getSession().get(USERNAME_SESSION);
        HandlerData data = new HandlerData();
        data.setAttribute("OP", "checkAdmin");
        data.setAttribute(USERNAME_SESSION, userName);

        data = userDAO.get(data);

        Result results = Results.html();

        if((boolean) data.getAttribute("isAdmin")) {
            results = Results.html().template("views/AdminController/sessions.admin.ftl.html").render("userName", userName);
        }

        data = new HandlerData();

        data.setAttribute("OP", "getSessionsForUser");
        data.setAttribute(USERNAME_SESSION, userName);

        HandlerData response = userSessionDAO.get(data);

        FlashScope scope = currContext.getFlashScope();

        if(data.getAttribute("errcode") == null) {
            results.render("sessionsList", (List<RegistredSession>) response.getAttribute("sessionsList"));
        } else {
            scope.error("sessions.error");
        }

        return results;
    }

    /**
     * Restituisce le informazioni dettagliate relative ad una specifica sessione di ricerca
     * che l'utente ha effettuato.
     *
     * @param sessionID id della sessione di ricerca
     * @param currContext contesto della richiesta
     * @return pagina con il dettaglio della sessione di ricerca richiesta
     */
    @FilterWith(RegistredUserFilter.class)
    public Result getSession(@PathParam(value="session_id") String sessionID, Context currContext) {
        String userName = currContext.getSession().get(USERNAME_SESSION);
        HandlerData data = new HandlerData();
        data.setAttribute("OP", "checkAdmin");
        data.setAttribute(USERNAME_SESSION, userName);

        data = userDAO.get(data);

        Map<String, Object> map = new HashMap<>();

        map.put("userName", userName);

        Result results;

        if((boolean) data.getAttribute("isAdmin")) {
            //map.put("analysisUrl", ninjaProperties.get("analysis.url"));
            results = Results.html().template("views/AdminController/session.admin.ftl.html");
        } else {
            results = Results.html().template("views/UserController/session.ftl.html");
        }

        data = new HandlerData();

        data.setAttribute("OP", "getSessionDetail");
        data.setAttribute(USERNAME_SESSION, userName);
        data.setAttribute(SEARCH_SESSION, sessionID);

        // session
        // readPagesList
        HandlerData response = userSessionDAO.get(data);

        FlashScope scope = currContext.getFlashScope();

        if(data.getAttribute("errcode") == null) {
            RegistredSession session = (RegistredSession) response.getAttribute("session");


            map.put("identifier", String.valueOf(session.getIdentifier()));
            map.put("searchTerm", session.getSearchTerm());
            map.put("day", String.valueOf(session.getDay()));
            map.put("month", String.valueOf(session.getMonth()));
            map.put("year", String.valueOf(session.getYear()));
            map.put("startSession", String.valueOf(session.getStartSession()));
            map.put("endSession", String.valueOf(session.getEndSession()));

            results.render(map);
            results.render("readPagesList", (List<WebPages>) response.getAttribute("readPagesList"));
        } else {
            scope.error("sessions.error");
        }

        return results;
    }

    /**
     * Metodo necessario per poter soddisfare una richiesta di tipo GET impiegata
     * per poter ritornare la pagina con i risultati di ricerca.
     *
     * @param searchQuery termine di ricerca immesso dall'utente
     * @param context contesto corrente della richiesta
     * @return pagina con i risultati che rispondono all'interrogazione formulata
     */
    public Result search(@Param(value="searchQuery") String searchQuery, Context context) {
        Result results = Results.html();
        Session session = context.getSession();

        // get search result
        HandlerData data = new HandlerData();

        data.setAttribute("searchQuery", searchQuery);
        data.setAttribute("limit", 10);
        data.setAttribute("OP", "getTopDocs");
        HandlerData response = anonimSessionDAO.get(data);

        List<WebPages> pagesList = (List<WebPages>) response.getAttribute("webPages");

        FlashScope scope = context.getFlashScope();

        if (response.getAttribute("errcode") != null) {
            scope.error("search.error");
            return results;
        }

        String loggedUser = session.get(USERNAME_SESSION);

        data.setAttribute("OP", "startSession");
        Calendar currDate = Calendar.getInstance();

        data.setAttribute("day", currDate.get(Calendar.DAY_OF_MONTH));
        data.setAttribute("month", currDate.get(Calendar.MONTH));
        data.setAttribute("year", currDate.get(Calendar.YEAR));


        // Registred user starts a new search session
        if(loggedUser != null) {
            String oldSearchSession = session.get(SEARCH_SESSION),
                    oldSearchQuery = session.get(SEARCH_QUERY_SESSION);

            boolean createNewSession = true;

            // Create a new search session
            if(oldSearchSession != null) {
                if(!oldSearchQuery.equals(searchQuery)) {
                     // ends previous session
                    HandlerData endsData = new HandlerData();

                    endsData.setAttribute("OP", "endsSession");
                    endsData.setAttribute(USERNAME_SESSION, loggedUser);
                    endsData.setAttribute(SEARCH_SESSION, oldSearchSession);
                    endsData.setAttribute("endHour", TimeUtils.getCurrentTime(currDate));

                    // We can ignore this result
                    // It MUST be correct, always...
                    userSessionDAO.insert(endsData);

                } else {
                    createNewSession = false;
                }

            }

            if(createNewSession) {
                data.setAttribute("userName", loggedUser);
                data.setAttribute("startHour", TimeUtils.getCurrentTime(currDate));

                response = userSessionDAO.insert(data);

                session.put("searchSession", (String) response.getAttribute("searchSession"));
                session.put("searchQuery", searchQuery);
            }

        } else { // anonymous user
            response = anonimSessionDAO.insert(data);
        }

        String errcode = (String) response.getAttribute("errcode");

        if(errcode != null) {
            scope.error("search.error");
        } else {
            scope.success("search.success");
            results.render("searchQuery", searchQuery);
            results.render("webPageList", pagesList);
        }

        return results;

    }

}
