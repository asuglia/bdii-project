package controllers;

import filters.LoggedAdmin;
import utils.TranslationUtils;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.utils.NinjaProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

/**
 * Controller impiegato per gestire tutte le richieste effettuate da un utente
 * che ha effettuato l'accesso come amministratore del sistema.
 *
 * Ognuno dei metodi può essere invocato solo ed esclusivamente nel momento in cui
 * è un utente amministratore ad effettuare la richiesta altrimenti l'applicazione
 * provvede ad effettuare un redirect verso una pagina di errore (403).
 */
@Singleton
public class AdminController {
    @Inject
    private NinjaProperties ninjaProperties;
    private static final Logger currLogger = Logger.getLogger(AdminController.class.getName());

    /**
     * Metodo impiegato per poter soddisfare una richiesta GET che ha lo scopo
     * di restituire la pagina che contiene il form da impiegare per la
     * configurazione del motore di riempimento.
     *
     * @return la pagina con il form di configurazione del motore di riempimento
     */
    @FilterWith(LoggedAdmin.class)
    public Result dbPopulator() {
        return Results.html(); //. render("analysisUrl", ninjaProperties.get("analysis.url"));
    }


    /**
     * Metodo impiegato per poter soddisfare una richiesta GET che ha lo scopo
     * di restituire la pagina che contiene il form da impiegare per l'inserimento
     * di una pagina di Wikipedia.
     *
     * @return la pagina con il form di inserimento di una nuova pagina
     */
    @FilterWith(LoggedAdmin.class)
    public Result insertPage() {
        return Results.html();//.render("analysisUrl", ninjaProperties.get("analysis.url"));
    }

    /**
     * Metodo impiegato per poter soddisfare una richiesta GET che ha lo scopo
     * di restituire la pagina che contiene i riferimenti alla piattaforma di analisi
     * del datawarehouse.
     *
     * @return la pagina con i riferimenti alla piattaforma di analisi
     */
    @FilterWith(LoggedAdmin.class)
    public Result analysis() {
        Map<String, String> analysisCubeUrl = new HashMap<>();

        analysisCubeUrl.put("analysisCube1", ninjaProperties.get("analysis.url1"));

        analysisCubeUrl.put("analysisCube2", ninjaProperties.get("analysis.url2"));


        return Results.html().render(analysisCubeUrl);
    }

}
