package controllers;

import dao.EntityDAO;
import dao.RegSessionAnnotation;
import dao.UserAnnotation;
import dao.HandlerData;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.params.Param;
import ninja.session.FlashScope;
import ninja.session.Session;
import ninja.validation.Validation;
import utils.TimeUtils;

import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

/**
 * Controller impiegato per poter gestire le richieste dell'utente
 * di login, logout o registrazione al sito
 */
@Singleton
public class LoginController {
    @Inject
    @UserAnnotation
    private EntityDAO userDAO;

    @Inject
    @RegSessionAnnotation
    private EntityDAO sessionRegDAO;

    private static Logger currLogger = Logger.getLogger(LoginController.class.getName());

    ///////////////////////////////////////////////////////////////////////////
    // Login
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Metodo impiegato per filtrare una richiesta GET necessaria per poter
     * ritornare la pagina principale di login.
     * @return la pagina HTML del login
     */
    public Result login() {

        return Results.html();

    }

    /**
     * Metodo impiegato per soddisfare una richiesta POST necessaria per poter
     * effettuare il login dell'utente con le credenziali specificate.
     *
     * @param userName nome utente specificato dall'utente
     * @param userPass password specificata dall'utente
     * @param context contesto della richiesta corrente
     * @return la pagina principale dell'utente oppure lo riporta alla pagina di login
     * con visualizzando un messaggio d'errore
     */
    public Result loginPost(@Param("userName") String userName,
                            @Param("userPassword") String userPass,
                            @Param("userIp") String userIp,
                            Context context) {

        HandlerData data = new HandlerData();
        data.setAttribute("OP", "login");
        data.setAttribute("userName", userName);
        data.setAttribute("userPassword", userPass);
        data.setAttribute("userIp", userIp);


        HandlerData response = userDAO.get(data);

        String isUserNameAndPasswordValid = (String) response.getAttribute("errcode");

        if (isUserNameAndPasswordValid == null) {
            context.getSession().put("userName", userName);
            context.getFlashScope().success("login.loginSuccessful");

            /*Fix pending session due to timeout previous session */
            data.clear();

            //fixNotFinishedSessions
            data.setAttribute("OP", "notFinishedSessions");
            data.setAttribute("userName", userName);

            response = sessionRegDAO.get(data);
            List<Long> sessionList = (List<Long>) response.getAttribute("sessions");

            if(!sessionList.isEmpty()) {
                data.clear();
                data.setAttribute("OP", "fixNotFinishedSessions");
                data.setAttribute("userName", userName);
                data.setAttribute("sessions", sessionList);
            }


            return Results.redirect("/");

        }

        // something is wrong with the input or password not found.
        context.getFlashScope().error("login.errorLogin");

        return Results.redirect("/login");


    }

    ///////////////////////////////////////////////////////////////////////////
    // Logout
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Metodo utilizzato per poter effettuare il logout dell'utente
     * il cui username è presente all'interno della sessione corrente.
     * @param context contesto della richiesta corrente
     * @return effettua un redirect alla pagina principale del sito
     */
    public Result logout(Context context) {
        Session session = context.getSession();
        HandlerData endsData = new HandlerData();
        String loggedUser = session.get("userName"),
                searchSession = session.get("searchSession");

        if(loggedUser != null && searchSession != null) {
            endsData.setAttribute("OP", "endsSession");
            endsData.setAttribute("userName", loggedUser);
            endsData.setAttribute("searchSession", searchSession);
            endsData.setAttribute("endHour", TimeUtils.getCurrentTime(Calendar.getInstance()));

            // We can ignore this result
            // It MUST be correct, always...
            sessionRegDAO.insert(endsData);
        }

        // remove any user dependent information
        context.getSession().clear();
        context.getFlashScope().success("login.logoutSuccessful");

        return Results.redirect("/");

    }

    /**
     * Metodo per poter restituire la pagina di registrazione in seguito
     * ad una richiesta GET
     * @return la pagina di registrazione al sito
     */
    public Result signup() {
        return Results.html();
    }

    /**
     * Metodo impiegato per gestire una richiesta POST utilizzata per inviare al
     * server i dati impiegati per effettuare la registrazione al sito.
     *
     * @param userName nome utente dell'utente che si sta registrando
     * @param userPass password dell'utente che si sta registrando
     * @param userEmail e-mail dell'utente che si sta registrando
     * @param context contesto della richiesta corrente
     * @return ritorna alla pagina di registrazione mostrando un messaggio che indica
     * l'esito dell'operazione
     */
    public Result signUpPost(@Param("userName") String userName,
                         @Param("userPassword") String userPass,
                         @Param("userEmail") String userEmail,
                         Context context) {
        HandlerData data = new HandlerData();

        data.setAttribute("OP", "signup");
        data.setAttribute("userName", userName);
        data.setAttribute("userPassword", userPass);
        data.setAttribute("userEmail", userEmail);

        data = userDAO.insert(data);
        String errCode = (String) data.getAttribute("errcode");

        FlashScope flashScope = context.getFlashScope();

        if(errCode != null) {
            flashScope.error("signup.error");
        } else {
            flashScope.success("signup.success");
        }

        return Results.redirect("/signup");
    }

}