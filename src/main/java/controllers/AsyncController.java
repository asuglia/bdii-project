package controllers;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import dao.*;
import dbpop.DBPopulationEngine;
import dbpop.DBPopulationEngineConf;
import dbpop.InvalidConfigurationException;
import filters.LoggedAdmin;
import dao.HandlerData;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.i18n.LangImpl;
import ninja.lifecycle.Dispose;
import ninja.session.Session;
import ninja.utils.NinjaProperties;
import utils.TranslationUtils;
import utils.WebPageJSON;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Logger;

/**
 * Controller che contiene la logica per gestire le richieste asincrone
 * provenienti dalla web application
 */
@Singleton
public class AsyncController {
    @Inject
    private DBPopulationEngine engine;
    @Inject
    private NinjaProperties ninjaProperties;
    private final ScheduledExecutorService executorService = Executors
            .newSingleThreadScheduledExecutor();
    @Inject
    private TranslationUtils translationUtils;
    private static final String SEARCH_SESSION = "searchSession";
    private static final Logger currLogger = Logger.getLogger(AdminController.class.getName());

    @Inject
    @RegSessionAnnotation
    private EntityDAO registredSessionDAO;
    @Inject
    @WebpageAnnotation
    private EntityDAO webpageDAO;


    /**
     * Filtra una richiesta di tipo POST necessaria per poter inserire una nuova
     * pagina di Wikipedia all'interno del sistema.
     *
     * La richiesta viene effettuata in maniera asincrona dalla web application
     *
     * @param context contesto della richiesta corrente
     * @return oggetto JSON con struttura {status:cod_errore}
     */
    @FilterWith(LoggedAdmin.class)
    public Result postInsertPage(final WebPageJSON pageJSON, final Context context) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Session currSession = context.getSession();
                Result result;
                HandlerData data = new HandlerData();

                data.setAttribute("OP", "insertPage");
                data.setAttribute("webpageURL", pageJSON.getWebsite());

                HandlerData response = webpageDAO.insert(data);

                String errCode = (String) response.getAttribute("errcode");
                String msg;

                if(errCode != null) {
                    msg = translationUtils.get(errCode);
                    result = Results.json().render("status", msg);
                } else {
                    msg = translationUtils.get("newpage.ok");
                    result = Results.json().render("status", msg);

                }
                context.returnResultAsync(result);
            }
        });

        return Results.async();

    }


    /**
     * Filtra una richiesta di tipo POST necessaria per poter registrare
     * l'avvenuta lettura di una certa pagina da un utente registrato.
     *
     * La richiesta viene effettuata in maniera asincrona dalla web application, pertanto
     * al termine dell'operazione viene effettuato un redirect alla pagina che l'utente ha visitato
     *
     * @param context contesto della richiesta corrente
     * @return oggetto JSON con struttura {status:cod_errore}
     */
    public Result readPage(final WebPageJSON pageJSON,
                           final Context context) {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Session currSession = context.getSession();
                String searchSession = currSession.get(UserController.SEARCH_SESSION),
                userName = currSession.get(UserController.USERNAME_SESSION);
                Result result = null;

                // registred user?
                if(userName != null) {
                    // Active search session
                    if (searchSession != null && pageJSON != null) {
                        HandlerData request = new HandlerData();
                        request.setAttribute("OP", "readPage");
                        request.setAttribute(UserController.SEARCH_SESSION, searchSession);
                        request.setAttribute(UserController.USERNAME_SESSION, userName);
                        request.setAttribute("pageUrl", pageJSON.getWebsite());

                        HandlerData response = registredSessionDAO.insert(request);


                        if (response.getAttribute("errcode") != null) {
                            result = Results.json().render("status", "readpage.error");
                        } else {
                            result = Results.json().render("status", "readpage.ok");
                        }


                    }
                } else {
                    // anonymous user haven't any problem
                    // Just redirect them to the selected website
                    result = Results.json().render("status", "readpage.ok");
                }

                context.returnResultAsync(result);
            }
        });

        return Results.async();

    }

    /**
     * Filtra una richiesta di tipo POST necessaria per poter avviare la procedura
     * di popolamento del database mediante il motore di riempimento.
     *
     * La richiesta viene effettuata in maniera asincrona dalla web application, pertanto
     * al termine dell'operazione non viene effettuato alcun redirect, bensì viene restituito
     * un JSON rappresentante lo stato finale della richiesta (popolamento errato o completato).
     *
     * @param configuration configurazione del motore di riempimento
     * @param context contesto della richiesta corrente
     * @return oggetto JSON con struttura {msg:msg_errore}
     */
    @FilterWith(LoggedAdmin.class)
    public Result postDbPopulator(final DBPopulationEngineConf configuration, final Context context) {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                String msg = "";

                if (configuration != null && configuration.isNotNull()) {
                    configuration.fixGenre(getCurrLanguage(context, ninjaProperties));

                    try {

                        engine.initConfiguration(configuration);
                        engine.startDBPopulation();

                        msg = translationUtils.get("dbpop.success");
                    } catch (InvalidConfigurationException e) {
                        msg = translationUtils.get("dbpop.conferror");
                        currLogger.severe(e.toString());
                    } catch (Exception e) {
                        msg = translationUtils.get("dbpop.error");
                        currLogger.severe(e.toString());

                    }
                } else {

                    msg = translationUtils.get("dbpop.conferror");
                    currLogger.severe("Configuration null or parameters null");
                }

                context.returnResultAsync(Results.json().render("msg", msg));
                currLogger.info("Returning async results...");

            }
        });


        return Results.async();
    }

    /**
     * Metodo di supporto del controller che viene invocato dal framework per poter
     * interrompere il servizio che si occupa di lanciare nuovi thread per poter
     * soddisfare le richieste asincrone
     */
    @Dispose
    public void shutdownExecutor() {
        executorService.shutdown();
    }

    /**
     * Metodo di supporto del controller impiegato per determinare la lingua correntemente
     * impiegata dall'utente che sta effettuando richieste alla web application.
     *
     * Il framework determina il suo valore seguendo una strategia ben precisa:
     * 1) Controllando se il risultato contiene una lingua impostata;
     * 2) Controllando se il contesto corrente presente un NINJA_LANG cookie con la lingua
     * impostata;
     * 3) Prelevando il primo linguaggio presente nel campo Accept-Language dell'header
     * della richiesta HTTP
     *
     * @param context contesto della richiesta corrente
     * @param properties proprietà della web application
     * @return identificativo della lingua in formato ISO
     */
    private String getCurrLanguage(Context context, NinjaProperties properties) {
        LangImpl lang = new LangImpl(properties);

        Optional<Result> resultOptional = Optional.absent();
        Optional<String> currLanguage = lang.getLanguage(context, resultOptional);

        return currLanguage.orNull();

    }

}
