package wiki.model;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Rappresenta la struttura informativa di un sito web di Wikipedia
 * e presenta delle funzionalità di supporto impiegate per poterle
 * recuperare dal Web
 */
public class WikipediaData implements SiteData {
    private String webpageTitle;
    private String text;
    private String siteUrl;
    private List<String> referenceList;
    private static final int MAX_REF = 5;
    private static final Logger currLogger = Logger.getLogger(WikipediaData.class.getName());

    public WikipediaData() {
        referenceList = new ArrayList<>();
    }

    /**
     * Recupera il titolo, il testo della pagina e 5 dei
     * riferimenti presenti al suo interno (casualmente).
     *
     * @param url URL della pagina da recuperare
     * @throws IOException
     */
    public void retrieveData(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        siteUrl = url;
        webpageTitle = doc.title();

        List<String> allRef = new ArrayList<>();
        int numReference = getWikiReference(doc, allRef);

        referenceList.addAll(selectRandomReference(allRef, numReference, MAX_REF));

        text = extractWebpageText(doc);
    }

    /**
     * Recupera il titolo, il testo della pagina.
     * Nel caso in cui il parametro supplementare sia true,
     * non recuperare i riferimenti ad essa associati.
     *
     * @param url URL della pagina da recuperare
     * @param noRef indica se è necessario recuperare i riferimenti associati alla pagina
     * @throws IOException
     */
    public void retrieveData(String url, boolean noRef) throws IOException {
        Document doc = Jsoup.connect(url).get();
        siteUrl = url;
        webpageTitle = doc.title();

        List<String> allRef = new ArrayList<>();
        if(!noRef) {
            int numReference = getWikiReference(doc, allRef);

            referenceList.addAll(selectRandomReference(allRef, numReference, MAX_REF));
        }

        text = extractWebpageText(doc);
    }

    /**
     * Restituisce l'URL del sito web
     * @return URL
     */
    public String getSiteUrl() {
        return siteUrl;
    }

    /**
     * Imposta l'URL della pagina corrente
     * @param siteUrl URL della pagina
     */
    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    /**
     * Restituisce il titolo della pagina
     * @return titolo della pagina
     */
    public String getTitle() {
        return webpageTitle;
    }

    /**
     * Restituisce il testo contenuto all'interno della pagina
     *
     * @return testo della pagina
     */
    public String getText() {
        return text;
    }

    /**
     * Lista dei riferimenti associati alla pagina
     * @return riferimenti della pagina
     */
    public List<String> getReferenceList() {
        return referenceList;
    }

    /**
     * Restituisce i riferimenti presenti all'interno della parte
     * principale della pagina di Wikipedia indicata dal tag con ID
     * "mw-content-text".
     *
     * @param doc istanza del documento dal quale si voglio acquisire i riferimenti
     * @param referenceList lista dei riferimenti da avvalorare
     * @return
     */
    private int getWikiReference(Document doc, List<String> referenceList) {
        /*
        * <a href="/wiki/Cat_pheromone" title="Cat pheromone">cat pheromones</a>
        * */
        String baseURL = "http://it.wikipedia.org";
        Elements paragraphs = doc.getElementById("mw-content-text").getElementsByTag("p");

        Elements allLinkTags = new Elements();

        for(Element para : paragraphs) {
            allLinkTags.addAll(para.getElementsByAttributeValueMatching("href", Pattern.compile("/wiki/[A-Za-z0-9%_]*")));
        }

        int numReference = 0;

        for(Element elem : allLinkTags) {
            String url = elem.attr("href");
            // Avoid pictures or supplementary webpages
            if(!url.matches("/wiki/.*:.*")) {
                referenceList.add(baseURL + elem.attr("href"));
                numReference++;
            }
        }

        return numReference;

    }

    /**
     * Seleziona in maniera del tutto casuale un certo numero di riferimenti
     * fra quelli specificati, restituendone una lista prima di duplicati.
     *
     * @param originalReference insieme di riferimenti originale
     * @param numReference numero di riferimenti recuperati
     * @param numRandom numero di riferimenti random richiesti
     * @return insieme di riferimenti
     */
    private Set<String> selectRandomReference(List<String> originalReference, int numReference, int numRandom) {
        if(numReference < numRandom)
            return new HashSet<>(originalReference);

        Random random = new Random();
        Set<String> randomReference = new HashSet<>();

        for(int i = 0; i < numRandom; i++) {
            randomReference.add(originalReference.get(random.nextInt(numRandom)));
        }

        return randomReference;
    }

    /**
     * Acquisisce il testo associato alla pagina web specificata
     * in input
     *
     * @param doc pagina web specificata
     * @return Testo della pagina
     */
    private String extractWebpageText(Document doc) {
        StringBuilder articleText = new StringBuilder("");

        try {
            articleText.append(ArticleExtractor.INSTANCE.getText(doc.body().html()));
        } catch (BoilerpipeProcessingException e) {
            currLogger.severe("Extracting article failed: " + e.getMessage());
        }

        return articleText.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WikipediaData that = (WikipediaData) o;

        return siteUrl.equals(that.siteUrl);

    }

    @Override
    public int hashCode() {
        return siteUrl.hashCode();
    }

    @Override
    public String toString() {
        return "WikipediaData{" +
                "webpageTitle='" + webpageTitle + '\'' +
                ", text='" + text + '\'' +
                ", siteUrl='" + siteUrl + '\'' +
                ", referenceList=" + referenceList +
                '}';
    }
}
