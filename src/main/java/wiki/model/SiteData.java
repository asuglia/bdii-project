package wiki.model;

import java.io.IOException;

/**
 * Interfaccia che rappresenta le operazioni principali che è possibile svolgere
 * su un determinato sito web
 */
public interface SiteData {
    /**
     * Avvalorare la struttura informativa dell'oggetto corrente
     * a partire dalle informazioni che è in grado di recuperare
     * dall'URL specificato
     *
     * @param url URL della pagina da recuperare
     * @throws IOException nel caso in cui vi siano problemi nella sua elaborazione
     */
    public void retrieveData(String url) throws IOException;

    /**
     * Restituisce il contenuto testuale della pagina
     * @return testo della pagina
     */
    public String getText();

    /**
     * Restituisce il titolo della pagina web
     * @return titolo della pagina
     */
    public String getTitle();
}
