package wiki.tokenizer;

import com.google.common.collect.Multiset;
import com.google.common.collect.TreeMultiset;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;

/**
 * Classe responsabile della suddivisione in token delle stringhe
 * di ricerca formulate.
 *
 * Impiega un ItalianAnalyzer di Lucene
 * (https://lucene.apache.org/core/5_0_0/analyzers-common/org/apache/lucene/analysis/it/ItalianAnalyzer.html)
 */
public class NaiveTokenizer implements PageTokenizer {
    private Analyzer analyzer;

    public NaiveTokenizer() {
        analyzer = new ItalianAnalyzer();

    }
    @Override
    public Multiset<String> tokenize(String text) throws IOException {
        TokenStream stream = analyzer.tokenStream("text", text);
        Multiset<String> tokenSet = TreeMultiset.create();

        stream.reset();

        while(stream.incrementToken()) {
            tokenSet.add(stream.getAttribute(CharTermAttribute.class).toString());
        }

        stream.end();
        stream.close();

        return tokenSet;

    }

    @Override
    public void close() {
        analyzer.close();
    }


}
