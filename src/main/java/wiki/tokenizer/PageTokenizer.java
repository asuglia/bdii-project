package wiki.tokenizer;

import com.google.common.collect.Multiset;

import java.io.IOException;

/**
 * Created by asuglia on 2/19/15.
 */
public interface PageTokenizer {
    public Multiset<String> tokenize(String text) throws IOException;
    public void close();
}
