package dao;

import com.google.common.collect.Multiset;
import com.google.inject.Inject;
import services.DBConnectionService;
import wiki.model.SiteData;
import wiki.tokenizer.PageTokenizer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Created by asuglia on 2/24/15.
 */
public class WebpageDAO implements EntityDAO {
    private static final Logger currLogger = Logger.getLogger(WebpageDAO.class.getName());
    private static final String FRONT_WEBPAGE = "webpageURL";
    private static final String INSERT_PAGE_QUERY = "insert into pagina (url, titolo) values(%s, %s);";
    private static final String INSERT_OCCURRENCES = "insert into occorrenze values(%s, %s, %s)";
    private static final String ERRCODE = "errcode";
    private static final String CHECK_PAGE_QUERY = "select url from pagina where url = %s";

    private final DBConnectionService databaseManager;
    private final SiteData webpageData;
    private final PageTokenizer pageTokenizer;

    @Inject
    public WebpageDAO(DBConnectionService databaseManager, SiteData webpageData, PageTokenizer pageTokenizer) {
        this.databaseManager = databaseManager;
        this.webpageData = webpageData;
        this.pageTokenizer = pageTokenizer;
    }


    /**
     * Non vi sono operazioni GET supportate
     * @param data parametro della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData get(HandlerData data) {
        return null;
    }

    /**
     * Operazioni INSERT supportate:
     * - insertPage
     *
     * @param data richiesta formulata
     * @return risultato della richiesta formulata
     */
    @Override
    public HandlerData insert(HandlerData data) {
        String operCode = (String) data.getAttribute("OP");

        switch(operCode) {
            case "insertPage":
                return insertPage(data);
            default:
                return null;
        }

    }

    @Override
    public HandlerData delete(HandlerData data) {
        return null;
    }


    /**
     * Provvede ad inserire i dati relativi ad una nuova pagina web
     * che il sistema dovrà gestirà. Preleva il suo contenuto dalla rete e
     * lo indicizza in maniera opportuna.
     *
     * @param data richiesta con url della pagina da elaborare
     * @return risultato dell'operazione
     */
    private HandlerData insertPage(HandlerData data) {
        HandlerData response = new HandlerData();
        String webpageURL = (String) data.getAttribute(FRONT_WEBPAGE);

        ResultSet set = null;

        try {
            String checkPage = String.format(CHECK_PAGE_QUERY,
                    databaseManager.escapeString(webpageURL));

            set = databaseManager.runSelectQuery(checkPage);

            // checks if the page is already present
            if(set.next()) {
                response.setAttribute(ERRCODE, "newpage.error");
                return response;
            }

            webpageData.retrieveData(webpageURL);

            currLogger.info("Get webpage data for " + webpageURL);

            String insertQuery = String.format(INSERT_PAGE_QUERY,
                    databaseManager.escapeString(webpageURL),
                    databaseManager.escapeString(webpageData.getTitle()));

            databaseManager.runDMLQuery(insertQuery);
            Multiset<String> tokens = pageTokenizer.tokenize(webpageData.getText());

            currLogger.info("Inserting tokens");
            for(String token : tokens.elementSet()) {

                String insertOccurences = String.format(INSERT_OCCURRENCES,
                        databaseManager.escapeString(webpageURL),
                        databaseManager.escapeString(token),
                        tokens.count(token));

                databaseManager.runDMLQuery(insertOccurences);
            }


        } catch (SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            response.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
        } catch (IOException e) {
            currLogger.severe("Download page error: " + e.getLocalizedMessage());
            response.setAttribute(ERRCODE, "newpage.error");
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {

                    currLogger.severe(e.getLocalizedMessage());
                    response.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
                }
            }
        }

        return response;
    }
}
