package dao;

import com.google.inject.Inject;
import models.WebPages;
import services.DBConnectionService;
import wiki.tokenizer.PageTokenizer;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Classe DAO necessaria per poter svolgere operazioni di accesso
 * alle sessioni
 */
@TableReference(tableName = "sessione")
public class SessionDAO implements EntityDAO {
    private static final Logger currLogger = Logger.getLogger(RegistredSessionDAO.class.getName());
    private static final String FRONT_DAY = "day";
    private static final String FRONT_MONTH = "month";
    private static final String FRONT_YEAR = "year";
    private static final String FRONT_SEARCH_QUERY = "searchQuery";
    private static final String URL_WEBPAGE = "url";
    private static final String TITLE_WEBPAGE = "titolo";
    private static final String FRONT_TOT_RESULTS = "limit";
    private static final String FRONT_WEB_PAGES = "webPages";
    private static final String ERRCODE = "errcode";

    private static final String RANKING_DOC_FUNCTION = "select p.url, p.titolo from pagina p order by rank_document_by_query(%s, p.url) desc limit %s;";
    private static final String INSERT_SESSION = "insert into sessione (giorno, mese, anno, termine_ricerca) values(%s, %s, %s, %s);";

    private final DBConnectionService databaseManager;
    private final PageTokenizer tokenizer;

    @Inject
    public SessionDAO(DBConnectionService databaseManager, PageTokenizer tokenizer) {
        this.databaseManager = databaseManager;
        this.tokenizer = tokenizer;
    }

    /**
     * Codici supportati per operazioni GET
     * - getTopDocs
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData get(HandlerData data) {
        String operCode = (String) data.getAttribute(SessionDAO.OPERATION_CODE);


        switch(operCode) {
            case "getTopDocs":
                return getTopDocs(data);

            default:
                return null;

        }
    }

    /**
     * Codici supportati per operazioni INSERT
     * - startSession
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData insert(HandlerData data) {
        String operCode = (String) data.getAttribute(SessionDAO.OPERATION_CODE);


        switch(operCode) {
            case "startSession":
                return startSession(data);
            default:
                return null;

        }
    }

    /**
     * Codici supportati per operazioni DELETE
     * Nessuna operazione disponibile
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData delete(HandlerData data) {
        return null;
    }

    private HandlerData startSession(HandlerData data) {
        HandlerData response = new HandlerData();

        String searchQuery = (String) data.getAttribute(FRONT_SEARCH_QUERY);

        int day = (int) data.getAttribute(FRONT_DAY),
                month = (int) data.getAttribute(FRONT_MONTH),
                year = (int) data.getAttribute(FRONT_YEAR);

        try {

            // insert session data
            String insertQuery = String.format(INSERT_SESSION, day,
                    month,
                    year,
                    databaseManager.escapeString(searchQuery));

            databaseManager.runDMLQuery(insertQuery);

        } catch (SQLException e) {
            response.setAttribute(ERRCODE, e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        }

        return response;
    }


    /**
     * Metodo impiegato per poter restituire i top-n documenti più rilevanti
     * rispetto alla query formulata.
     *
     * @param data richiesta formulata
     * @return risultato della richiesta formulata
     */
    private HandlerData getTopDocs(HandlerData data) {
        int maxSearchResults = (int) data.getAttribute(FRONT_TOT_RESULTS);
        String searchQuery = (String) data.getAttribute(FRONT_SEARCH_QUERY);

        ResultSet set = null;
        HandlerData response = new HandlerData();

        try {
            String tokenizedQuery = getTokenizedQuery(searchQuery),
                    nativeQuery = String.format(RANKING_DOC_FUNCTION,
                            databaseManager.escapeString(tokenizedQuery),
                            maxSearchResults);
            set = databaseManager.runSelectQuery(nativeQuery);
            List<WebPages> webPagesList = new ArrayList<>();
            while (set.next()) {
                webPagesList.add(new WebPages(set.getString(URL_WEBPAGE), set.getString(TITLE_WEBPAGE)));
            }

            response.setAttribute(FRONT_WEB_PAGES, webPagesList); // adds the webpages to the response
        } catch (IOException e) {
            response.setAttribute("errcode", e.getLocalizedMessage());
            currLogger.severe(e.getLocalizedMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return response;
    }

    /**
     * Suddivide in token normalizzati la stringa fornita in input
     *
     * @param query query da tokenizzare
     * @return stringa che presenta i token normalizzati separati da uno spazio
     * @throws IOException
     */
    private String getTokenizedQuery(String query) throws IOException {
        StringBuilder builder = new StringBuilder("");

        for(String s : tokenizer.tokenize(query)) {
            builder.append(s).append(" ");
        }

        return builder.toString();
    }

}
