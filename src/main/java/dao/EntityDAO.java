package dao;


/**
 * Classe DAO utilizzata per poter effettuare generiche operazioni di
 * accesso al database
 */
public interface EntityDAO {
    public static final String OPERATION_CODE = "OP";

    public HandlerData get(HandlerData data);
    public HandlerData insert(HandlerData data);
    public HandlerData delete(HandlerData data);
}
