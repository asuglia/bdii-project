package dao;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by asuglia on 2/17/15.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface TableReference {
    String tableName();
}
