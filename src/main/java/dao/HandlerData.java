package dao;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe di supporto che viene impiegata per poter
 * trasferire i dati da un modulo del sistema ad un altro
 * in maniera del tutto trasparente rispetto al suo contenuto
 */
public class HandlerData {
    private Map<String, Object> handlerAttribute;

    /**
     * Construisce questo oggetto avvalorando le strutture
     * impiegate internamente
     */
    public HandlerData() {
        handlerAttribute = new HashMap<>();
    }

    /**
     * Imposta come valore quello specificato ad una determinata chiave
     * @param key chiave della coppia
     * @param value valore associato alla chiave
     * @param <T> Generico tipo che è possibile associare ad una certa chiave
     */
    public <T> void setAttribute(String key, T value) {
        handlerAttribute.put(key, value);
    }

    /**
     * Restituisce il valore associato alla chiave specificata
     * @param key chiave
     * @return valore associato alla chiave
     */
    public Object getAttribute(String key) {
        return handlerAttribute.get(key);
    }


    /**
     * Distrugge il contenuto della struttura corrente
     */
    public void clear() {
        handlerAttribute.clear();
    }

    /**
     * Distrugge l'elemento avente come chiave quella specificata.
     *
     * Nel caso in cui la chiave non fosse presente, non verrebbe
     * effettuata alcuna modifica alla struttura
     *
     * @param key chiave dell'elemento da rimuovere
     */
    public void remove(String key) {
        try {
            handlerAttribute.remove(key);
        }catch(NullPointerException e){
            // silently do nothing
        }
    }

    @Override
    public String toString() {
        return handlerAttribute.toString();
    }
}
