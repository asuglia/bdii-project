package dao;

import com.google.inject.Inject;
import models.*;
import services.DBConnectionService;
import utils.TimeUtils;
import wiki.tokenizer.PageTokenizer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

/**
 * Classe DAO che contiene le operazioni di manipolazione di una
 * sessione per un utente registrato
 */
@TableReference(tableName = "sessione_utente_reg")
public class RegistredSessionDAO implements EntityDAO {
    private static final Logger currLogger = Logger.getLogger(RegistredSessionDAO.class.getName());
    private static final String FRONT_USERNAME = "userName";
    private static final String FRONT_DAY = "day";
    private static final String FRONT_MONTH = "month";
    private static final String FRONT_YEAR = "year";
    private static final String FRONT_START_HOUR = "startHour";
    private static final String FRONT_END_HOUR = "endHour";
    private static final String FRONT_SEARCH_QUERY = "searchQuery";
    private static final String FRONT_READPAGE_TIME = "time";
    private static final String FRONT_WEBPAGE = "pageUrl";
    private static final String FRONT_TOT_RESULTS = "limit";
    private static final String FRONT_WEB_PAGES = "webPages";
    private static final String URL_WEBPAGE = "url";
    private static final String TITLE_WEBPAGE = "titolo";
    private static final String USERNAME = "username";
    private static final String SESSION = "codice";
    private static final String TIME_READPAGE = "time";
    private static final String FRONT_SEARCH_SESSION = "searchSession";
    private static final String RANKING_DOC_FUNCTION =
            "select p.url, p.titolo from pagina p order by rank_document_by_query(%s, p.url) desc limit %s;";
    private static final String INSERT_SESSION =
            "insert into sessione (giorno, mese, anno, termine_ricerca) values(%s, %s, %s, %s) returning codice;";
    private static final String INSERT_SESSION_REG =
            "insert into sessione_utente_reg (codice, utente, ora_inizio) values(%s, %s, %s)";
    private static final String ERRCODE = "errcode";
    private static final String INSERT_READ_PAGE = "insert into pagine_lette (utente, pagina, sessione) values(%s, %s, %s)";
    private static final String SESSIONS_FOR_USER =
            "select s_reg.codice, s_reg.ora_inizio, s_reg.ora_fine, s.giorno, s.mese, s.anno, s.termine_ricerca " +
                    "from sessione_utente_reg s_reg, sessione s where s_reg.ora_fine is not null and " +
                    "s_reg.codice = s.codice and s_reg.utente = %s";
    private static final String ENDS_HOUR_QUERY =
            "update sessione_utente_reg set ora_fine = %s where utente = %s and codice = %s;";
    private static final String SESSION_DETAIL =
     "select s_reg.codice, s_reg.ora_inizio, s_reg.ora_fine, s.giorno, s.mese, s.anno, s.termine_ricerca" +
            " from sessione s, sessione_utente_reg s_reg " +
            "where s.codice = s_reg.codice and s_reg.ora_fine is not null and s_reg.codice = %s and s_reg.utente = %s";

    private static final String READ_PAGES_DETAIL =
            "select p.url, p.titolo from pagina p, pagine_lette pl where pl.sessione = %s and pl.utente = %s and pl.pagina = p.url";
    private static final String DELETE_ALL_SESSIONS_FOR_USER =
            "delete from sessione_utente_reg s_reg where utente = %s and s_reg.ora_fine is not null";
    private static final String NOT_FINISHED_SESSIONS_FOR_USER =
            "select codice from sessione_utente_reg where utente=%s and ora_fine is null";

    private final DBConnectionService databaseManager;
    private final PageTokenizer tokenizer;

    @Inject
    public RegistredSessionDAO(DBConnectionService databaseManager, PageTokenizer tokenizer) {
        this.databaseManager = databaseManager;
        this.tokenizer = tokenizer;
    }

    /**
     * Codici supportati per operazioni GET
     * - getSessionsForUser
     * - getSessionDetail
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData get(HandlerData data) {
        String operCode = (String) data.getAttribute(OPERATION_CODE);


        switch(operCode) {
            case "getSessionsForUser":
                return getSessionsForUser(data);

            case "getSessionDetail":
                return getSessionDetail(data);
            case "notFinishedSessions":
                return notFinishedSessions(data);
            default:
                return null;

        }

    }

    /**
     * Codici supportati per operazioni INSERT
     * - startSession
     * - endsSession
     * - readPage
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData insert(HandlerData data) {
        String operCode = (String) data.getAttribute(OPERATION_CODE);


        switch(operCode) {
            case "startSession":
                return startSession(data);

            case "endsSession":
                return endsSession(data);

            case "readPage":
                return readPage(data);

            case "fixNotFinishedSessions":
                return fixNotFinishedSessions(data);
            default:
                return null;

        }
    }

    /**
     * Codici supportati per operazioni DELETE
     * - deleteSessionsForUser
     *
     * @param data parametri della richiesta
     * @return risultato della richiesta
     */
    @Override
    public HandlerData delete(HandlerData data) {
        String operCode = (String) data.getAttribute(OPERATION_CODE);


        switch(operCode) {
            case "deleteSessionsForUser":
                return deleteSessionsForUser(data);
            default:
                return null;

        }
    }

    /**
     * Cancella tutte le sessioni associate ad un certo utente
     * @param data richiesta formulata contenente l'username dell'utente
     * @return risultato dell'operazione
     */
    private HandlerData deleteSessionsForUser(HandlerData data) {
        HandlerData response = new HandlerData();

        String userName = (String) data.getAttribute(FRONT_USERNAME);

        try {
            String deleteQuery = String.format(DELETE_ALL_SESSIONS_FOR_USER,
                    databaseManager.escapeString(userName));

            databaseManager.runDMLQuery(deleteQuery);


        } catch(SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            response.setAttribute("errcode", e.getErrorCode());
        }

        return response;
    }

    /**
     * Restituisce i dettagli di una sessione dell'utente specificato
     *
     * @param data dati della sessione e dell'utente specificato
     * @return informazioni complete sulla sessione
     */
    private HandlerData getSessionDetail(HandlerData data) {
        HandlerData response = new HandlerData();
        String userName = (String) data.getAttribute(FRONT_USERNAME),
                sessionId = (String) data.getAttribute(FRONT_SEARCH_SESSION);

        ResultSet set = null;

        try {

            String sessioneQuery = String.format(SESSION_DETAIL,
                    sessionId,
                    databaseManager.escapeString(userName));

            currLogger.info(sessioneQuery);

            set = databaseManager.runSelectQuery(sessioneQuery);

            RegistredSession session = new RegistredSession();
            while(set.next()) {
                session.setIdentifier(set.getLong("codice"));
                session.setStartSession(set.getTime("ora_inizio"));
                session.setEndSession(set.getTime("ora_fine"));
                session.setSearchTerm(set.getString("termine_ricerca"));
                session.setDay(set.getInt("giorno"));
                session.setMonth(set.getInt("mese"));
                session.setYear(set.getInt("anno"));
            }

            set.close();
            response.setAttribute("session", session);

            String readPagesQuery = String.format(READ_PAGES_DETAIL, sessionId,
                    databaseManager.escapeString(userName));
            List<WebPages> readPagesList = new ArrayList<>();

            set = databaseManager.runSelectQuery(readPagesQuery);
            while(set.next()) {
                readPagesList.add(new WebPages(set.getString("url"), set.getString("titolo")));
            }

            response.setAttribute("readPagesList", readPagesList);

        } catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return response;
    }

    /**
     * Restituisce la lista di sessioni associate all'utente specificato
     *
     * @param data identificatore dell'utente che ha effettuato la richiesta
     * @return lista delle sessioni dell'utente
     */
    private HandlerData getSessionsForUser(HandlerData data) {
        HandlerData response = new HandlerData();
        String userName = (String) data.getAttribute(FRONT_USERNAME);

        ResultSet set = null;

        try {
            String sessionsQuery = String.format(SESSIONS_FOR_USER,
                    databaseManager.escapeString(userName));

            set = databaseManager.runSelectQuery(sessionsQuery);

            List<RegistredSession> sessionsList = new ArrayList<>();

            while(set.next()) {
                RegistredSession session = new RegistredSession();
                session.setIdentifier(set.getLong("codice"));
                session.setStartSession(set.getTime("ora_inizio"));
                session.setEndSession(set.getTime("ora_fine"));
                session.setSearchTerm(set.getString("termine_ricerca"));
                session.setDay(set.getInt("giorno"));
                session.setMonth(set.getInt("mese"));
                session.setYear(set.getInt("anno"));
                sessionsList.add(session);
            }

            response.setAttribute("sessionsList", sessionsList);

        } catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return response;
    }


    /**
     * Effettua l'inserimento dei dati relativi all'avvenuta lettura di una
     * certa pagina da parte di un utente.
     *
     * @param data dati relativi alla pagina letta da uno specifico utente
     * @return risultato dell'operazione
     */
    private HandlerData readPage(HandlerData data) {
            String searchSession = (String) data.getAttribute(FRONT_SEARCH_SESSION),
                    userName = (String) data.getAttribute(FRONT_USERNAME),
                pageUrl = (String) data.getAttribute(FRONT_WEBPAGE);


        HandlerData response = new HandlerData();
        
        try {
            String insertQuery = String.format(INSERT_READ_PAGE,
                    databaseManager.escapeString(userName),
                    databaseManager.escapeString(pageUrl),
                    searchSession);

            databaseManager.runDMLQuery(insertQuery);

        } catch (SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            response.setAttribute("errcode", e.getErrorCode());
        } catch (Exception e) {
            currLogger.severe(e.getLocalizedMessage());
        }

        return response;
    }

    private HandlerData fixNotFinishedSessions(HandlerData data) {
        List<Long> sessionsID = (List<Long>) data.getAttribute("sessions");
        String userName = (String) data.getAttribute(FRONT_USERNAME);
        HandlerData response = new HandlerData();

        try {


            for(Long id : sessionsID) {
                String query = String.format(ENDS_HOUR_QUERY,
                        databaseManager.escapeString(TimeUtils.getCurrentTime(Calendar.getInstance())),
                                databaseManager.escapeString(userName),
                                id);

                databaseManager.runDMLQuery(query);
            }

        }catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        }

        return response;
    }

    private HandlerData notFinishedSessions(HandlerData data) {
        HandlerData response = new HandlerData();
        String userName = (String) data.getAttribute(FRONT_USERNAME);
        ResultSet set = null;

        try {
            String notFinishedQuery = String.format(NOT_FINISHED_SESSIONS_FOR_USER,
                    databaseManager.escapeString(userName));

            set = databaseManager.runSelectQuery(notFinishedQuery);

            List<Long> notFinishedSessionID = new ArrayList<>();

            while(set.next()) {
                notFinishedSessionID.add(set.getLong(SESSION));
            }

            response.setAttribute("sessions", notFinishedSessionID);

        } catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }



        return response;
    }


    /**
     * Inserisce i dati necessari per porre fine ad una sessione iniziata da un
     * certo utente.
     *
     * @param data dati dell'utente e della sessione da egli iniziata
     * @return risultato dell'operazione
     */
    private HandlerData endsSession(HandlerData data) {
        HandlerData response = new HandlerData();

        String userName = (String) data.getAttribute(FRONT_USERNAME),
                searchSession = (String) data.getAttribute(FRONT_SEARCH_SESSION),
                endHour = (String) data.getAttribute(FRONT_END_HOUR);

        try {

            String insertEndHour = String.format(ENDS_HOUR_QUERY,
                    databaseManager.escapeString(endHour),
                    databaseManager.escapeString(userName),
                    searchSession);

            databaseManager.runDMLQuery(insertEndHour);

        } catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        }

        return response;
    }

    /**
     * Inserisce i dati necessari per rappresentare una sessione iniziata da un
     * certo utente.
     *
     * @param data dati dell'utente e della sessione da egli iniziata
     * @return risultato dell'operazione
     */
    private HandlerData startSession(HandlerData data) {
        HandlerData response = new HandlerData();

        String searchQuery = (String) data.getAttribute(FRONT_SEARCH_QUERY),
                userName = (String) data.getAttribute(FRONT_USERNAME),
                startTime = (String) data.getAttribute(FRONT_START_HOUR);

        int day = (int) data.getAttribute(FRONT_DAY),
                month = (int) data.getAttribute(FRONT_MONTH),
                year = (int) data.getAttribute(FRONT_YEAR);

        Session currSession = new Session(searchQuery, day, month, year);
        ResultSet set = null;

        try {

            // insert session data
            String insertQuery = String.format(INSERT_SESSION, day,
                    month,
                    year,
                    databaseManager.escapeString(searchQuery));

            // Gets the resulting id from the insert operation
            set = databaseManager.runDMLQueryWithResult(insertQuery);
            if(set == null) {
                response.setAttribute("errcode", "search.error");
                currLogger.severe("No returning data from result set");
                return response;
            }

            while(set.next()) {
                currSession.setIdentifier(set.getLong(SESSION));
            }

            // Insert registred session
            String insertSRQuery = String.format(INSERT_SESSION_REG, currSession.getIdentifier(), 
                    databaseManager.escapeString(userName),
                    databaseManager.escapeString(startTime));
            
            databaseManager.runDMLQuery(insertSRQuery);

            response.setAttribute(FRONT_SEARCH_SESSION, String.valueOf(currSession.getIdentifier()));

        } catch(SQLException e) {
            response.setAttribute("errcode", e.getErrorCode());
            currLogger.severe(e.getLocalizedMessage());
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();    
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return response;
    }


}
