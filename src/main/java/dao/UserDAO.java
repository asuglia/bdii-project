package dao;

import services.DBConnectionService;
import models.User;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.persistence.PersistenceException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Logger;

/**
 * Classe DAO che rappresenta le operazioni di accesso al database per gli utenti
 * della web application
 */
@Singleton
public class UserDAO implements EntityDAO {
    private static final String FRONT_USERIP = "userIp";
    @Inject
    private DBConnectionService databaseManager;
    private MessageDigest encrypter;

    private static final Logger currLogger = Logger.getLogger(UserDAO.class.getName());

    public static final String USER_ENTITY = "entity";
    public static final String FRONT_USERNAME = "userName";
    public static final String FRONT_PASSWORD = "userPassword";
    public static final String FRONT_EMAIL = "userEmail";
    public static final String FRONT_ROOT = "isAdmin";
    public static final String FRONT_VALIDATION = "validation";

    public static final String USERNAME = "userName";
    public static final String PASSWORD = "userPassword";
    public static final String EMAIL = "userEmail";
    public static final String ROOT = "isAdmin";
    public static final String ERRCODE = "errcode";
    public static final String OPERATION_CODE = "OP";

    private static final String IS_ADMIN_QUERY = "select * from utente u where u.username = %s and u.root = true";
    private static final String IS_AUTHORIZED_USER = "select * from utente u where u.username = %s and u.password = %s";
    private static final String SIGNUP_NEW_USER = "insert into utente values(%s,%s,%s)";
    private static final String SIGNUP_VALID_CREDENTIAL = "select u.username, u.email from utente u where u.username=%s and u.email=%s";
    private static final String INSERT_HISTORY_IP = "insert into identificato values(%s, %s, %s);";

    /**
     * Istanzia l'oggetto DAO corrente e la predispone per poter
     * elaborare le operazioni da effettuare sul database
     */
    public UserDAO() {
        try {
            encrypter = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo impiegato per gestire le richieste che prevedono
     * il recupero delle informazioni dal database.
     * L'operazione da svolgere viene determinata dal parametro OP specificato
     * nella richiesta.
     *
     * @param data richiesta da poter a termine
     * @return dati risultanti dall'operazione
     */
    @Override
    public HandlerData get(HandlerData data) {
        String operationID = (String) data.getAttribute(OPERATION_CODE);
        HandlerData response = new HandlerData();

        switch (operationID) {
            case "login":
                response = doLogin(data);
                break;
            case "getEntity":
                response = getEntity(data);
                break;
            case "checkAdmin":
                response = isAdmin(data);
                break;
        }

        return response;

    }

    /**
     * Metodo impiegato per gestire le richieste che prevedono
     * l'inserimento di informazioni nel database per la tabella
     * che contiene le informazioni degli utenti.
     * L'operazione da svolgere viene determinata dal parametro OP specificato
     * nella richiesta.
     *
     * @param data richiesta da poter a termine
     * @return dati risultanti dall'operazione
     */
    @Override
    public HandlerData insert(HandlerData data) {
        String operationID = (String) data.getAttribute(OPERATION_CODE);
        HandlerData response = null;

        switch(operationID) {
            case "signup":
                response = signUpOperation(data);
                break;

        }

        return response;
    }

    /**
     * Effettua la cancellazione di un utente dal database
     *
     * @param data richiesta di cancellazione di un utente
     * @return dati risultanti dalla richiesta
     */
    @Override
    public HandlerData delete(HandlerData data) {
        HandlerData response = new HandlerData();
        User user = (User) response.getAttribute(USER_ENTITY);

        try {
            databaseManager.runSelectQuery("");
        } catch(SQLException e) {
            response.setAttribute(ERRCODE, e.getErrorCode());
        }

        return response;
    }

    /**
     * Metodo di supporto che effettua l'operazione di recupero
     * delle informazioni associate ad un certo utente, conoscendo
     * il suo username
     *
     * @param data dati della richiesta
     * @return dati risultanti dalla richiesta
     */
    private HandlerData getEntity(HandlerData data) {
        HandlerData returnData = new HandlerData();

        String userName = (String) data.getAttribute(FRONT_USERNAME);

        User returnUser = null;

        try {
            //returnUser = databaseManager.find(User.class).where().eq(USERNAME, userName).findUnique();
        } catch (PersistenceException e) { // unable to find user, set it to null
            returnUser = null;
            returnData.setAttribute(ERRCODE, String.valueOf(((SQLException) e.getCause()).getErrorCode()));
        }

        // returns the retrieved user
        returnData.setAttribute("entity", returnUser);

        return returnData;
    }

    /**
     * Controlla che l'utente specificato possieda i privilegi
     * di amministratore per la web application.
     *
     * @param data richiesta
     * @return risultato richiesta
     */
    private HandlerData isAdmin(HandlerData data) {
        HandlerData returnData = new HandlerData();

        String userName = (String) data.getAttribute(FRONT_USERNAME);

        ResultSet set = null;
        try {
            String currQuery = String.format(IS_ADMIN_QUERY,
                    databaseManager.escapeString(userName));


            set = databaseManager.runSelectQuery(currQuery);

            returnData.setAttribute(FRONT_ROOT, set.next());

        } catch (SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            returnData.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {

                    currLogger.severe(e.getLocalizedMessage());
                    returnData.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
                }
            }
        }

        return returnData;
    }

    /**
     * Porta a termine l'operazione di login di un utente
     * specificando le sue credenziali.
     *
     * @param data richiesta di login
     * @return risultato della richiesta
     */
    private HandlerData doLogin(HandlerData data) {
        HandlerData response = new HandlerData();
        String userName = (String) data.getAttribute(FRONT_USERNAME),
                userPass = new String(encrypter.digest(((String) data.getAttribute(FRONT_PASSWORD)).getBytes())),
                userIp = (String) data.getAttribute(FRONT_USERIP);

        ResultSet set = null;
        try {
            String currQuery = String.format(IS_AUTHORIZED_USER, databaseManager.escapeString(userName) ,
                    databaseManager.escapeString(userPass));

            currLogger.info("Executing " + currQuery);
            set = databaseManager.runSelectQuery(currQuery);

            if(!set.next()) {
                response.setAttribute(ERRCODE, "user.login.userpass");
            }

            // record new ip address
            String ipAddressQuery = String.format(INSERT_HISTORY_IP,
                    databaseManager.escapeString(userName),
                    databaseManager.escapeString(String.valueOf(new Timestamp(Calendar.getInstance().getTimeInMillis()))),
                    databaseManager.escapeString(userIp));

            currLogger.info("Inserting ip address: " + ipAddressQuery);

            databaseManager.runDMLQuery(ipAddressQuery);

        } catch (SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            response.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {

                    currLogger.severe(e.getLocalizedMessage());
                    response.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
                }
            }
        }

        return response;
    }

    /**
     * Porta a termine l'operazione di registrazione di un utente
     * specificando le sue credenziali.
     *
     * @param data richiesta di registrazione
     * @return risultato della richiesta
     */
    private HandlerData signUpOperation(HandlerData data) {
        HandlerData response = new HandlerData();


        String userName = (String) data.getAttribute(FRONT_USERNAME),
         userPass = new String(encrypter.digest(((String) data.getAttribute(FRONT_PASSWORD)).getBytes())),
        userEmail = (String) data.getAttribute(FRONT_EMAIL);

        ResultSet set = null;
        boolean isDone = false;
        try {
            String currSelectQuery = String.format(SIGNUP_VALID_CREDENTIAL,
                    databaseManager.escapeString(userName), databaseManager.escapeString(userEmail)),
                    currInsertQuery = String.format(SIGNUP_NEW_USER,
                            databaseManager.escapeString(userName),
                            databaseManager.escapeString(userPass),
                            databaseManager.escapeString(userEmail));

            currLogger.info("Executing " + currSelectQuery);

            set = databaseManager.runSelectQuery(currSelectQuery);

            if(!set.next()){
                databaseManager.runDMLQuery(currInsertQuery);
            } else {
                response.setAttribute(ERRCODE, "signup.errmsg");

            }
        } catch (SQLException e) {
            currLogger.severe(e.getLocalizedMessage());
            response.setAttribute(ERRCODE, String.valueOf(e.getErrorCode()));
        } finally {
            if(set != null) {
                try {
                    if(!set.isClosed()) {
                        set.close();
                    }
                } catch (SQLException e) {
                    currLogger.severe(e.getLocalizedMessage());
                }
            }
        }

        return response;
    }

}
