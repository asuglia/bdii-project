package services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import ninja.lifecycle.Dispose;
import ninja.lifecycle.Start;
import ninja.utils.NinjaProperties;

import java.sql.*;
import java.util.logging.Logger;

/**
 * Created by asuglia on 2/22/15.
 */
@Singleton
public class DBConnectionService {
    @Inject
    private Connection connection;
    @Inject
    private NinjaProperties properties;

    @Dispose(order=10)
    public void startService() {

    }

    @Dispose(order = 10)
    public void stopService() throws SQLException {
        this.connection.close();
    }

    public ResultSet runSelectQuery(String query) throws SQLException {
        Statement stat = connection.createStatement();
        return stat.executeQuery(query);
    }

    public boolean runDMLQuery(String query) throws SQLException {
        Statement stat = connection.createStatement();
        return stat.execute(query);
    }

    public ResultSet runDMLQueryWithResult(String query) throws SQLException {
        Statement stat = connection.createStatement();

        return (stat.execute(query)) ? stat.getResultSet() : null;
    }

    public String escapeString(String param) {
        return "'" + param + "'";
    }
}
