package models;

/**
 * Classe POJO che rappresenta una sessione generica
 */
public class Session implements Model {
    protected Long identifier;

    protected String searchTerm;

    protected int day;

    protected int month;

    protected int year;

    public Session() {
    }

    public Session(String searchTerm, int day, int month, int year) {
        this.searchTerm = searchTerm;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    protected Session(Long identifier, String searchTerm, int day, int month, int year) {
        this.searchTerm = searchTerm;
        this.identifier = identifier;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Long identifier) {
        this.identifier = identifier;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
