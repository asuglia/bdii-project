package models;

import java.sql.Time;
import java.util.List;

/**
 * Classe POJO che rappresenta la sessione di un utente registrato
 */
public class RegistredSession extends Session {
    private User user;

    private Time startSession;

    private Time endSession;

    private List<WebPages> webpages;

    public RegistredSession() {
    }

    public RegistredSession(User user, String searchTerm, int day, int month, int year, String startSession) {
        this.user = user;
        this.startSession = Time.valueOf(startSession);
    }

    public void setStartSessionString(String start) {
        this.startSession = Time.valueOf(start);
    }

    public void setEndSessionString(String end) {
        this.endSession = Time.valueOf(end);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Session getSession() {
        return new Session(this.identifier, this.searchTerm, this.day, this.month, this.year);
    }

    public Time getStartSession() {
        return startSession;
    }

    public void setStartSession(Time startSession) {
        this.startSession = startSession;
    }

    public Time getEndSession() {
        return endSession;
    }

    public void setEndSession(Time endSession) {
        this.endSession = endSession;
    }
}
