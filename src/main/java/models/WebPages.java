package models;

/**
 * Classe POJO che rappresenta la struttura di una pagina web
 * gestita dal sistema
 */
public class WebPages implements Model {
    private String URL;
    private String title;

    public WebPages() {
    }

    public WebPages(String URL, String title) {
        this.URL = URL;
        this.title = title;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
