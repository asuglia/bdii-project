package models;

import java.util.List;

/**
 * Classe che rappresenta la struttura informativa di un utente
 * della web application
 */
public class User implements Model {
    private String userName;
    private String userPassword;
    private boolean isAdmin;
    private String userEmail;

    private List<RegistredSession> sessions;

    public List<RegistredSession> getSessions() {
        return sessions;
    }

    public void setSessions(List<RegistredSession> sessions) {
        this.sessions = sessions;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public User() {
    }

    public User(String userName, String userPassword, boolean isAdmin, String userEmail) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.isAdmin = isAdmin;
        this.userEmail = userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean value) {
        this.isAdmin = value;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

}
